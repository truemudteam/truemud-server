﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TrueMUD.Database.Commands;
using TrueMUD.Database.ConnectionTypes;

namespace TrueMUD.Database
{
    public class DatabaseManager : IDisposable
    {
        private  IDatabase _dataBase;

        public DatabaseManager(IDatabase database)
        {
            _dataBase = database;
        }

        public bool Connect(string address, string user, string password, string database, string instance, ConnectionProtocols protocol)
        {
            return _dataBase.Connect(address, user, password, database, instance, protocol);
        }
        
        public void Execute(UpdateCommand command)
        {
            _dataBase.Update(command);
        }

        public void Execute(InsertCommand command)
        {
            _dataBase.Insert(command);

        }

        public void BulkUpdate(string db, string table, Queue<Dictionary<string, string>> updates)
        {
            _dataBase.BulkUpdate(db, table, updates);
        }

        public List<Dictionary<string, object>> Execute(SelectCommand command)
        {
            List<Dictionary<string, object>> returnTable = new List<Dictionary<string, object>>();

            DataTable dt = _dataBase.Select(command);

            for (int r = 0; r < dt.Rows.Count; r++)
            {
                Dictionary<string, object> tmpRow = new Dictionary<string, object>();

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    tmpRow.Add(dt.Columns[i].ColumnName, dt.Rows[r][dt.Columns[i].ColumnName]);
                }

                returnTable.Add(tmpRow);
            }

            return returnTable;
        }

        public void Dispose()
        {
            _dataBase.Disconnect();
        }

        public int Execute(DeleteCommand command)
        {
            return _dataBase.Delete(command);
        }
    }
}
