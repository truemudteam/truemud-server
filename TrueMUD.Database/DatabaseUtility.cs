﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace TrueMUD.Database
{
    public static class DatabaseUtility
    {
        private static char _delimiter = '^';
        private static char _keySeparater = '=';
        public static Dictionary<ConnectionProtocols, string> Protocols = new Dictionary<ConnectionProtocols, string>();

        static DatabaseUtility()
        {
            Protocols.Add(ConnectionProtocols.TCP, "protocol=tcp");
            Protocols.Add(ConnectionProtocols.PIPE, "protocol=pipe;PipeName=MYSQLPIPE");
            Protocols.Add(ConnectionProtocols.MEMORY, "protocol=memory;shared memory name=MYSQL");

        }
        public static  List<Dictionary<string, int>> ConvertToArray(string input)
        {
            List<Dictionary<string, int>> output = new List<Dictionary<string, int>>();

            string[] sOut = input.Split(_delimiter);

            for(int i = 0; i < sOut.Length; i+=2)
            {
                Dictionary<string, int> keyOutput = new Dictionary<string,int>();

                string[] keyValue = sOut[i].Split(_keySeparater);
                
                keyOutput.Add(keyValue[0], int.Parse(keyValue[1]));

                output.Add(keyOutput);
            }

            return output;
        }

        public static string[] GetColumns(DataTable data)
        {
            
            List<string> columns = new List<string>();

            foreach(DataColumn col in data.Columns)
            {
                columns.Add(col.ColumnName);
            }

            return columns.ToArray();
        }
    }
}
