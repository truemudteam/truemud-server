﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueMUD.Database.Commands
{
    public class SelectCommand : BaseCommand
    {
        private bool _selectAllColumns = false;

        public SelectCommand(string table, string[] columns, string[] parameters,
            bool selectAll = false, bool allowPartial = false)
            : base(table, columns, parameters)
        {
            Type = "Select Command";
            AllowPartial = allowPartial;

            _selectAllColumns = selectAll;
        }


        public override string Execute()
        {
            if (Columns != null)
            {
                string columnSelect = string.Format("\"{0}\"", string.Join("\",\"", Columns));
                string valueSelect = string.Empty;

                string selectCommand = string.Empty;

                if (Parameters != null)
                {
                    if (Parameters.Length > 0)
                    {
                        List<string> columnWhere = new List<string>();

                        for (int i = 0; i < Columns.Length; i++)
                        {
                            if (AllowPartial == false)
                            {
                                columnWhere.Add(string.Format("{0}.{1}='{2}'", Table, Columns[i], Parameters[i]));
                            }
                            else
                            {
                                columnWhere.Add(string.Format("{0}.{1} RLIKE '{2}'", Table, Columns[i], Parameters[i]));
                            }
                        }

                        valueSelect = string.Join(" AND ", columnWhere.ToArray());


                        if (_selectAllColumns == true && Parameters[0] == "*")
                        {
                            return string.Format("SELECT * FROM \"{0}\";", Table);
                        }

                        if (_selectAllColumns == true)
                        {
                            return string.Format("SELECT * FROM {0} WHERE {1};", Table, valueSelect);
                        }

                        //SELECT * FROM spells WHERE spells.id='1';

                        string outputQuery = string.Format("SELECT {0} FROM {1} WHERE {2};", columnSelect, Table, valueSelect);
                        
                        return outputQuery;
                    }
                }
            }

            if(UseWhereQuery == true)
            {
                return string.Format("SELECT * FROM {0} WHERE {1};", Table, WhereQuery);
            }


            return string.Format("SELECT * FROM {0};", Table);
        }
    }
}
