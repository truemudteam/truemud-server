﻿
namespace TrueMUD.Database.Commands
{
    public abstract class BaseCommand
    {
        public string Table;
        public string[] Columns { get; private set; }
        protected string[] Parameters;
        protected string Type;
        protected bool AllowPartial = false;
        public string Database = "mud-db";
        public string WhereQuery = "";
        public bool UseWhereQuery = false;


        public BaseCommand(string table, string[] columns, string[] parameters)
        {
            Table = table;
            Columns = columns;
            Parameters = parameters;
        }


        public abstract string Execute();
    }
}
