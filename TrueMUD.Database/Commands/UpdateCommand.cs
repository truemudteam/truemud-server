﻿using System.Collections.Generic;

namespace TrueMUD.Database.Commands
{
    public class UpdateCommand : BaseCommand
    {
        private string _where = string.Empty;

        public UpdateCommand(string table, string[] columns, string[] parameters, string where) : base(table, columns, parameters)
        {
            Type = "Update Command";
            _where = where;
        }

        public override string Execute()
        {
       //UPDATE `mud-db`.`rooms` SET `roomitemids`='{11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23}' WHERE  `id`=1;
            List<string> valueEntry = new List<string>();

            for (int i = 0; i < Columns.Length; i++)
            {
                int tmpInt = 0;

                bool isValue = int.TryParse(Parameters[i], out tmpInt);

                if (isValue == true)
                {
                    valueEntry.Add(string.Format("`{0}`={1}", Columns[i], Parameters[i]));
                }
                else
                {
                    valueEntry.Add(string.Format("`{0}`='{1}'", Columns[i], Parameters[i]));
                }
            }


            return string.Format("UPDATE `{0}`.`{1}` SET {2} WHERE {3};", Database, Table, string.Join(",", valueEntry), _where);
        }

        public static string GetUpdateCommand(string table, string db, string[] columns, string[] parameters, string where)
        {
            List<string> valueEntry = new List<string>();

            for (int i = 0; i < columns.Length; i++)
            {
                int tmpInt = 0;

                bool isValue = int.TryParse(parameters[i], out tmpInt);

                if (isValue == true)
                {
                    valueEntry.Add(string.Format("`{0}`={1}", columns[i], parameters[i]));
                }
                else
                {
                    valueEntry.Add(string.Format("`{0}`='{1}'", columns[i], parameters[i]));
                }
            }


            return string.Format("UPDATE `{0}`.`{1}` SET {2} WHERE {3};", db, table, string.Join(",", valueEntry), where);
        }
    }
}
