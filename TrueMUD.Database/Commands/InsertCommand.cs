﻿using System.Collections.Generic;

namespace TrueMUD.Database.Commands
{
    public class InsertCommand : BaseCommand
    {
        public InsertCommand(string table, string[] columns, string[] parameters) 
            : base(table, columns, parameters)
        {
            Type = "Insert Command";
        }

        public override string Execute()
        {
            List<string> columnEntry = new List<string>();
            List<string> valueEntry = new List<string>();

            

            for (int i = 0; i < Columns.Length; i++)
            {
                columnEntry.Add(string.Format("`{0}`", Columns[i]));
            }

            for (int i = 0; i < Parameters.Length; i++)
            {
                int tmpInt = 0;

                bool isValue = int.TryParse(Parameters[i], out tmpInt);

                if (isValue == true)
                {
                    valueEntry.Add(string.Format("{0}", Parameters[i]));
                }
                else
                {
                    valueEntry.Add(string.Format("'{0}'", Parameters[i]));
                }
            }

            return string.Format("INSERT INTO {0} ({1}) VALUES ({2});", Table, string.Join(",", columnEntry), string.Join(", ", valueEntry));
        }
    }
}
