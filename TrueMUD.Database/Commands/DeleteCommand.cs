﻿using System;

namespace TrueMUD.Database.Commands
{
    public class DeleteCommand : BaseCommand
    {
        public DeleteCommand(string table, string[] columns, string[] parameters) 
            : base(table, columns, parameters)
        {
            Type = "Delete Command";
        }
        public override string Execute()
        {
            return string.Format("DELETE FROM `{0}`.`{1}` WHERE `id`={2}", Database, Table, Parameters[0]);
        }
    }
}
