﻿using System.Collections.Generic;
using System.Data;
using TrueMUD.Database.Commands;

namespace TrueMUD.Database.ConnectionTypes
{
    public interface IDatabase
    {
        void BulkUpdate(string db, string table, Queue<Dictionary<string, string>> updates);
        bool Update(BaseCommand command);
        DataTable Select(BaseCommand command);
        bool Connect(string address, string user, string password, string database, string instance, ConnectionProtocols protocol);
        void Disconnect();
        int Insert(InsertCommand command);
        int Delete(DeleteCommand command);
    }
}
