﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using TrueMUD.Database.Commands;

namespace TrueMUD.Database.ConnectionTypes
{
    public class MySqlDatabase : IDatabase
    {
        private MySqlConnection _connection;
        private string _connectionString = string.Empty;
        private string _instanceID = string.Empty;
        private Socket _updateSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);


        public bool Connect(string address, string user, string password, string database, string instance, ConnectionProtocols protocol)
        {
            _connectionString = string.Format("server={0};uid={1};pwd={2};database={3};{4};", address, user, password, database, DatabaseUtility.Protocols[protocol]);

            _instanceID = instance;
            try
            {
                _connection = new MySqlConnection();
                _connection.ConnectionString = _connectionString;
                _connection.Open();

                _updateSocket.Connect("127.0.0.1", 11000);
                

                Console.WriteLine(_connection.State.ToString());

                return _connection.State == System.Data.ConnectionState.Open;

            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                return false;
            }
        }

        private void SendToUpdateServer(string query)
        {
            _updateSocket.Send(Encoding.ASCII.GetBytes(query));
        }

        private void UpdateTask(string db, string table, Queue<Dictionary<string, string>> updates)
        {
           
                var tmpUpdate = new Queue<Dictionary<string, string>>(updates);

                foreach (var update in tmpUpdate)
                {
                if (update != null)
                {
                    if (update.ContainsKey("id") == true)
                    {
                        string id = update["id"];

                        update.Remove("id");

                        string updateQuery = UpdateCommand.GetUpdateCommand(table, db, update.Keys.ToArray(), update.Values.ToArray(), string.Format("id={0}", id));

                        SendToUpdateServer(updateQuery);
                    }
                }
                }

               
            
            updates.Clear();
        }

        public void BulkUpdate(string db, string table, Queue<Dictionary<string, string>> updates)
        {
            //new TaskFactory().StartNew(() =>UpdateTask(db, table, updates));
            UpdateTask(db, table, updates);
        }

        public void Disconnect()
        {
            _connection.Close();
            _updateSocket.Close();

        }

        private static object SelectLock = new object();

        public DataTable Select(BaseCommand command)
        {
            //_connection.Open();
            lock (SelectLock)
            {
                MySqlDataAdapter adapter = new MySqlDataAdapter(command.Execute(), _connection);
                
                DataSet dataSet = new DataSet(command.Table);

                Thread.Sleep(10);

                if (command.Table.Length > 0)
                {
                    adapter.Fill(dataSet, command.Table);

                    DataTable table = dataSet.Tables[command.Table];

                    return table;
                }

                return null;
            }
        }

        public bool Update(BaseCommand command)
        {

            SendToUpdateServer(command.Execute());

            return true;
        }


        public int Insert(InsertCommand command)
        {
            try
            {
                SendToUpdateServer(command.Execute());

                return 1;
            }
            catch(Exception ex)
            {
                 return 0;
            }
        }


        public int Delete(DeleteCommand command)
        {

            SendToUpdateServer(command.Execute());

            return 1;
        }
    }
}
