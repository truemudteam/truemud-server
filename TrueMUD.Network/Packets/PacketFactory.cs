﻿using System.Collections.Generic;

namespace TrueMUD.Network.Packets
{
    public static class PacketFactory
    {
        public static BasePacket GetPacket(PacketTypes packetType, Dictionary<string, string> parameters)
        {
            switch(packetType)
            {
                case PacketTypes.ANSI:
                    return new ANSIPacketType(parameters);

                case PacketTypes.Default:
                default:
                    return new DefaultPacketType(parameters);              
            }
        }
    }
}
