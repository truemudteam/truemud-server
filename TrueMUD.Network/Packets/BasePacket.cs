﻿using System.Collections.Generic;
using System.Text;

namespace TrueMUD.Network.Packets
{
    public abstract class BasePacket
    {
        protected byte[] PacketData { get; set; }
        public abstract void Encode(Dictionary<string, string> parameters);
        
        public byte[] GetData()
        {
            return PacketData;
        }
    }
}
