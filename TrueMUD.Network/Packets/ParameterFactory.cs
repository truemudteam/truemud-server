﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueMUD.Network.Packets
{
    public static class ParameterFactory
    {
        private static Dictionary<string, string> _parameters = new Dictionary<string, string>();

        public static Dictionary<string, string> GetParameters(PacketTypes packetType)
        {
            _parameters.Clear();

            switch (packetType)
            {
                case PacketTypes.ANSI:                
                    _parameters.Add(ParameterConstants.INPUT, "");
                    _parameters.Add(ParameterConstants.OUTPUT, "");
                    _parameters.Add(ParameterConstants.ARGS, "");

                    return _parameters;
            }

            return _parameters;
        }

        public static object[] ConvertToObjArray(string inputArgs)
        {
            string[] args = inputArgs.Split(',');

            return args;
        }
    }
}
