﻿using System.Collections.Generic;
using System.Text;

namespace TrueMUD.Network
{
    public class ANSIUtility
    {
        public static byte[] EscapeSequence = new byte[] { 0x1B, 0x5B };
        public static byte[] Newline = new byte[] { 0x0D, 0x0A };
        public const byte AnsiDelimiter = 0x3B;

        public static byte[] ClearScreen = new byte[] { EscapeSequence[0], EscapeSequence[1], 0x32, 0x4A };
        public static byte[] ClearLine = new byte[] { EscapeSequence[0], EscapeSequence[1], 0x32, 0x4B };
        public static byte[] CursorDown = new byte[] { 0x1B, 0x5B, 0x42 };

        // Create our color table
        private static List<AnsiData> ansiTable = new List<AnsiData>();
        public static readonly Dictionary<int, byte> Ascii;

        /// <summary>
        /// Our static constructor is used to prefill our color table, so that we do not need
        /// to do so at runtime.
        /// </summary>
        static ANSIUtility()
        {
            Ascii = new Dictionary<int, byte>
            {
                {'─', 196},
                {'Ç', 128},
                {'ü', 129},
                {'é', 130},
                {'│', 179},
                {'┤', 180},
                {'«', 174},
                {'»', 175},
                {'┌', 218},
                {'┐', 191},
                {'├', 195},
                {'└', 192},
                {'┘', 217},
                {'◄', 17},
                {'⌐', 169},
                {'┴', 193}
            };

            for (byte i = 0; i < 0x80; i++)
            {
                Ascii.Add(i, i);
            }

            // Our reset values turns everything to the default mode
            ansiTable.Add(new AnsiData("$reset$", "\x1B[0m", "Reset", false, 0));

            // Style Modifiers (on)
            ansiTable.Add(new AnsiData("$bold$", "\x1B[1m", "Bold", false, 0));
            ansiTable.Add(new AnsiData("$italic$", "\x1B[3m", "Italic", false, 0));
            ansiTable.Add(new AnsiData("$ul$", "\x1B[4m", "Underline", false, 0));
            ansiTable.Add(new AnsiData("$blink$", "\x1B[5m", "Blink", false, 0));
            ansiTable.Add(new AnsiData("$blinkf$", "\x1B[6m", "Blink Fast", false, 0));
            ansiTable.Add(new AnsiData("$inverse$", "\x1B[0;7m", "Inverse", false, 0));
            ansiTable.Add(new AnsiData("$strike$", "\x1B[9m", "Strikethrough", false, 0));

            // Style Modifiers (off)
            ansiTable.Add(new AnsiData("$!bold$", "\x1B[0m", "Bold Off", false, 0));
            ansiTable.Add(new AnsiData("$!italic$", "\x1B[23m", "Italic Off", false, 0));
            ansiTable.Add(new AnsiData("$!ul$", "\x1B[24m", "Underline Off", false, 0));
            ansiTable.Add(new AnsiData("$!blink$", "\x1B[25m", "Blink Off", false, 0));
            ansiTable.Add(new AnsiData("$!inverse$", "\x1B[27m", "Inverse Off", false, 0));
            ansiTable.Add(new AnsiData("$!strike$", "\x1B[29m", "Strikethrough Off", false, 0));

            // Foreground Color
            ansiTable.Add(new AnsiData("$black$", "\x1B[30m", "Foreground black", false, 0));
            ansiTable.Add(new AnsiData("$red$", "\x1B[31m", "Foreground red", false, 0));
            ansiTable.Add(new AnsiData("$green$", "\x1B[32m", "Foreground green", false, 0));
            ansiTable.Add(new AnsiData("$yellow$", "\x1B[33m", "Foreground yellow", false, 0));
            ansiTable.Add(new AnsiData("$blue$", "\x1B[34m", "Foreground blue", false, 0));
            ansiTable.Add(new AnsiData("$magenta$", "\x1B[35m", "Foreground magenta", false, 0));
            ansiTable.Add(new AnsiData("$cyan$", "\x1B[36m", "Foreground cyan", false, 0));
            ansiTable.Add(new AnsiData("$white$", "\x1B[37m", "Foreground white", false, 0));

            // Background Color
            ansiTable.Add(new AnsiData("$!black$", "\x1B[40m", "Background black", false, 0));
            ansiTable.Add(new AnsiData("$!red$", "\x1B[41m", "Background red", false, 0));
            ansiTable.Add(new AnsiData("$!green$", "\x1B[42m", "Background green", false, 0));
            ansiTable.Add(new AnsiData("$!yellow$", "\x1B[43m", "Background yellow", false, 0));
            ansiTable.Add(new AnsiData("$!blue$", "\x1B[44m", "Background blue", false, 0));
            ansiTable.Add(new AnsiData("$!magenta$", "\x1B[45m", "Background magenta", false, 0));
            ansiTable.Add(new AnsiData("$!cyan$", "\x1B[46m", "Background cyan", false, 0));
            ansiTable.Add(new AnsiData("$!white$", "\x1B[47m", "Background white", false, 0));

            ansiTable.Add(new AnsiData("$clear_line$", "\x1B[K", "Clear Line", false, 0));
            ansiTable.Add(new AnsiData("$clear_screen$", "\x1B[2J", "Clear the entire Screen", false, 0));
            ansiTable.Add(new AnsiData("$move_cursor_left$", "\x1B[{0}D", "Clear left x amount of spaces.", true, 1));
            ansiTable.Add(new AnsiData("$move_cursor$", "\x1B[{0};{1}f", "move cursor", true, 2));




        } // End of AnsiColor

        public static byte[] GetBytes(string inputString)
        {
            byte[] outputData = new byte[inputString.Length];

            for (int i = 0; i < inputString.Length; i++)
            {
                outputData[i] = Ascii[inputString[i]];
            }

            return outputData;
        }

        public static byte[] Ansify(string stringToColor, string arguments)
        {
            return GetBytes(Ansify(stringToColor, arguments.Split(',')));
        }

        public static string Ansify(string stringToColor, object[] arguments = null)
        {
            // Loop through our table
            foreach (AnsiData ansiData in ansiTable)
            {
                // Replace our identifier with our code
                stringToColor = stringToColor.Replace(ansiData.Identifier, ansiData.Code);

                if (ansiData.HasArguments == true)
                {
                    if (arguments != null)
                    {
                        if (arguments.Length == ansiData.numberOfArguments)
                        {
                            stringToColor = string.Format(stringToColor, arguments);
                        }
                    }
                }
            }
            // Return our colored string
            return (stringToColor);
        } // End of Colorize Function

        private struct AnsiData
        {
            #region Private Variables

            private readonly string identifier;
            private readonly string code;
            private readonly string definition;
            public bool HasArguments;
            public int numberOfArguments;

            #endregion

            #region Public Properties

            public string Code
            {
                get { return code; }
            } // End of ReadOnly Code

            public string Identifier
            {
                get { return identifier; }
            } // End of ReadOnly Identifier

            public string Definition
            {
                get { return definition; }
            } // End of ReadOnly Definition

            #endregion

            public AnsiData(string identifier, string code, string definition, bool hasArguments, int numberOfArgs)
            {
                // Set our values
                this.identifier = identifier;
                this.code = code;
                this.definition = definition;
                this.numberOfArguments = numberOfArgs;
                this.HasArguments = hasArguments;
            } // End of ColorData Constructor

        } // End of ColorData structure
    }
}