﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrueMUD.Network.Packets
{
    public class ANSIPacketType : BasePacket
    {
        public ANSIPacketType()
        {
        }

        public ANSIPacketType(Dictionary<string, string> parameters)
        {
            Encode(parameters);
        }

        public override void Encode(Dictionary<string, string> parameters)
        {
            PacketData = ANSIUtility.Ansify(parameters[ParameterConstants.INPUT], parameters[ParameterConstants.ARGS]);
        }
    }
}
