﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueMUD.Network.Packets
{
    public class ParameterConstants
    {
        public const string INPUT = "input";
        public const string OUTPUT = "output";
        public const string ARGS = "args";
    }
}
