﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueMUD.Network.Packets
{
    public class DefaultPacketType : BasePacket
    {
        public DefaultPacketType(Dictionary<string, string> parameters)
        {
            Encode(parameters);
        }

        public override void Encode(Dictionary<string, string> parameters)
        {
            PacketData = Encoding.ASCII.GetBytes(parameters[ParameterConstants.INPUT]);
        }
    }
}
