﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TrueMUD.Network.Packets;

namespace TrueMUD.Network
{
    public static class Server
    {
        const int PORT_NO = 23;
        const string SERVER_IP = "127.0.0.1";
        private static TcpListener Listener;
        public static IPEndPoint EndPoint { get; private set; }
      
        public static void Start(IPEndPoint endPoint)
        {
            ConnectionManager.Start();
            
            Listener = new TcpListener(endPoint);
            Listener.Start();
            EndPoint = (IPEndPoint)Listener.LocalEndpoint;
            
            new TaskFactory().StartNew(() => ListenForClients());
            new TaskFactory().StartNew(() => MainLoop());
        }

        public static void MainLoop()
        {
            while(true)
            {
                ProcessClientOutgoingMessages();
                Thread.Sleep(1);
            }
        }

        public static void ListenForClients()
        {
            while(true)
            {
                Console.WriteLine("Waiting for clients.");

                TcpClient client = Listener.AcceptTcpClient();

                Console.WriteLine("Client connected, routing...");

                ConnectionManager.AddConnection(client);

                Thread.Sleep(5);
            }
        }

        public static void ProcessClientOutgoingMessages()
        {
            ConnectionManager.ProcessClientOutgoingMessages();
        }
    }
}
