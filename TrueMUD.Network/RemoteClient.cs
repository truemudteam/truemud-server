﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using TrueMUD.Game;
using TrueMUD.Game.Entity;
using TrueMUD.Network.Packets;

namespace TrueMUD.Network
{
    public class RemoteClient
    {
        public PlayerEntity Player;
        public string ID = string.Empty;
        private TcpClient _clientConnection = null;
        private byte[] _txData = null;
        private NetworkStream _networkStream;
        public Queue<byte[]> OutgoingServerPackets = new Queue<byte[]>();

        public RemoteClient(TcpClient connection)
        {
            ID = Guid.NewGuid().ToString();
            _clientConnection = connection;
            _networkStream = _clientConnection.GetStream();

            Dictionary<string, string> parameters = ParameterFactory.GetParameters(PacketTypes.ANSI);
            parameters[ParameterConstants.INPUT] = Game.GameManager.WelcomeMessage;

            ANSIPacketType aNSIPacketType = new ANSIPacketType(parameters);

            OutgoingServerPackets.Enqueue(aNSIPacketType.GetData());

            parameters = ParameterFactory.GetParameters(PacketTypes.ANSI);
            parameters[ParameterConstants.INPUT] = string.Join("\n", GameManager.OnlinePlayers);

            aNSIPacketType = new ANSIPacketType(parameters);

        }

        public void ProcessOutgoingPackets()
        {
            if(OutgoingServerPackets.Count > 0)
            {
                _txData = OutgoingServerPackets.Dequeue();

                _networkStream.Write(_txData, 0, _txData.Length);
            }

            if (Player != null)
            {
                _txData = Player.OutgoingMessages.GetMessage();

                if (_txData.Length > 0)
                {
                    _networkStream.Write(_txData, 0, _txData.Length);
                }
            }
        }
    }
}
