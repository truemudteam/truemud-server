﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TrueMUD.Network;

namespace TrueMUD.Network
{
    public static class ConnectionManager
    {
        public static ConcurrentDictionary<string, RemoteClient> ClientConnections = new ConcurrentDictionary<string, RemoteClient>();
        private static ConcurrentQueue<TcpClient> _pendingConnections = new ConcurrentQueue<TcpClient>();
        private static List<string> _bannedAddressList = new List<string>();

        public static void Start()
        {
            new TaskFactory().StartNew(() => ProcessIncomingClients());
        }

        public static void AddConnection(TcpClient client)
        {
            if (_bannedAddressList.Contains(client.Client.RemoteEndPoint.ToString()) == false)
            {
                _pendingConnections.Enqueue(client);
                Console.WriteLine("Connection Address {0} is not banned, adding to server.", client.Client.RemoteEndPoint.ToString());
            }
            else
            {
                client.Client.Send(Encoding.ASCII.GetBytes("This address is banned."));
                client.Close();
                client = null;
            }
        }

        public static void ProcessIncomingClients()
        {
            while(true)
            {
                if(_pendingConnections.Count > 0)
                {
                    TcpClient client = null;

                    _pendingConnections.TryDequeue(out client);

                    RemoteClient remoteClient = new RemoteClient(client);

                    ClientConnections.TryAdd(remoteClient.ID, remoteClient);

                    Console.WriteLine("Client {0} has been added.", remoteClient.ID);
                }

                Thread.Sleep(10);
            }
        }

        public static void ProcessClientOutgoingMessages()
        {
            if(ClientConnections.Count > 0)
            {
                foreach(var remoteClient in ClientConnections)
                {
                    remoteClient.Value.ProcessOutgoingPackets();
                }
            }
        }
    }
}
