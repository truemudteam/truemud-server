﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueMUD.Database.Commands;
using TrueMUD.Database.ConnectionTypes;

namespace TrueMUD.Game.Managers
{
    public static class GameDatabaseManager
    {
        private static Database.DatabaseManager DatabaseManager = new Database.DatabaseManager(new MySqlDatabase());

        public static void Initialize()
        {
            DatabaseManager.Connect("127.0.0.1", "muduser", "water98", "mud-db", "0", Database.ConnectionProtocols.TCP);
        }

        public static List<Dictionary<string, object>> Select(SelectCommand command, bool local)
        {
            if (local == false)
            {
                return DatabaseManager.Execute(command);
            }
            else
            {
                return new List<Dictionary<string, object>>();
                // get data from the local database.
            }
        }
    }
}
