﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueMUD.Database.Commands;
using TrueMUD.Game.Constants;
using TrueMUD.Game.Entity;

namespace TrueMUD.Game.Managers
{
    public static class PlayerManager
    {
        public static Dictionary<int, PlayerEntity> Players = new Dictionary<int, PlayerEntity>();


        public static void Initialize()
        {          
            GameDatabaseManager.Select(GameConstants.GameQueries.selectAllPlayers, false);
        }
    }
}
