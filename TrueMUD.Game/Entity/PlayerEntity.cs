﻿using System.Collections.Generic;
using TrueMUD.Game.Network;

namespace TrueMUD.Game.Entity
{
    public class PlayerEntity
    {
        public PacketHandler OutgoingMessages = new PacketHandler();

        public PlayerEntity(int id)
        {
            OutgoingMessages.Add(string.Join("\n", GameManager.OnlinePlayers));
        }
    }
}
