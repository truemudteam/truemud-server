﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueMUD.Game.Network
{
    public class PacketHandler
    {
        private Queue<byte[]> _outgoingMessages = new Queue<byte[]>();

        public byte[] GetMessage()
        {
            if (_outgoingMessages.Count > 0)
            {
                return _outgoingMessages.Dequeue();
            }

            return new byte[0];
        }

        public void Add(string message)
        {
            _outgoingMessages.Enqueue(Encoding.ASCII.GetBytes(message));
        }
    }
}
