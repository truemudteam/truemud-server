﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueMUD.Game.Entity;
using TrueMUD.Game.Managers;

namespace TrueMUD.Game
{
    public static class GameManager
    {
        public static string WelcomeMessage = string.Empty;
        public static List<string> OnlinePlayers = new List<string>();

        public static void Initialize()
        {
            GameDatabaseManager.Initialize();

            PlayerManager.Initialize();

            WelcomeMessage = "$reset$$bold$$magenta$Welcome to TrueMUD!";

            var playersOnline = GameDatabaseManager.Select(Game.Constants.GameConstants.GameQueries.selectAllPlayers, false);

            foreach(var player in playersOnline)
            {
                OnlinePlayers.Add(player["name"].ToString());
            }
        }
    }
}
