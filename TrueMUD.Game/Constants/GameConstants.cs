﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueMUD.Database.Commands;

namespace TrueMUD.Game.Constants
{
    public class GameConstants
    {
        public class GameQueries
        {
            public static SelectCommand selectAllPlayers = new SelectCommand("players", new string[] { "id" }, null, true, false);
        }
    }
}
