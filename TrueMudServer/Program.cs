﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TrueMUD.Game;
using TrueMUD.Network;

namespace TrueMudServer
{
    class Program
    {
        static void Main(string[] args)
        {
            GameManager.Initialize();

            Server.Start(new System.Net.IPEndPoint(IPAddress.Parse("127.0.0.1"), 23));

            Console.Read();
        }
    }
}
